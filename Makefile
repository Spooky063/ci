.PHONY: lint
lint: vendor/autoload.php
	vendor/bin/phpstan analyse

.PHONY: test
test: vendor/autoload.php
	vendor/bin/phpunit

vendor/autoload.php: composer.lock
	composer install