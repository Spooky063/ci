<?php

namespace App\Tests;

use App\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    public function testNameCorrect()
    {
        $person = (new Person())
            ->setName('John')
        ;
        $this->assertEquals('John', $person->getName());
    }
}
